package gt;

/**
 *   classe immuable Fraction qui représente un nombre
 *rationnel. Une fraction comporte un numérateur et un 
 *dénominateur (nombres entiers).
 */
public final class Fraction 
{
	 
   private int denominateur,numerateur;
   
   public Fraction(int n,int d)
   {
	   this.numerateur=n;
	   this.denominateur=d;
   }
   
   public Fraction(int n)
   {
	   this.numerateur=n;
	   this.denominateur=1;
   }
   
   public Fraction()
   {
	   this.numerateur=0;
	   this.denominateur=1;
   }
   
   
  final Fraction ZERO= new Fraction(0,1);
  final Fraction UN= new Fraction(1,1);
  
  double getFractionValue() {return (double)this.numerateur/this.denominateur;}
  
  void affichageValue()
  {
	  System.out.println(this.getFractionValue());
	  
	  
  }
   
   void affichage() 
   {
	   System.out.println(this.denominateur +" "+ this.numerateur);
	   
   }
   

   /*
    * chaine de caractère
    * 
    * */
   
   public String toString()
   {
	   return String.valueOf(this.numerateur)+"/"+String.valueOf(this.denominateur);
   }
   
  


   public static void main(String args []) 
   {
	   
	   System.out.println("hallo deutscheland");
	   
   }
}


